import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { EmbryoService } from '../../../Services/Embryo.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-DetailPage',
  templateUrl: './DetailPage.component.html',
  styleUrls: ['./DetailPage.component.scss']
})
export class DetailPageComponent implements OnInit {

   @Input() productId : any ;
   
   product : any ;

   Stores : any ;

   id                : any;
   type              : any;
   apiResponse       : any;
   singleProductData : any;
   productsList      : any;

   constructor(private route: ActivatedRoute,
              private router: Router,
              public embryoService: EmbryoService) {
                 this.Stores = []
     
   }

   ngOnInit() {
      this.route.params.subscribe(res => {
         this.id = res.id;
         this.type = res.type;
         //this.getData();
         this.getProduct(this.id)
      })
   }

   public getData() {
      this.embryoService.getProducts().valueChanges().subscribe(res => this.checkResponse(res));
   }

   public checkResponse(response) {
      this.productsList = null;
      this.productsList = response[this.type];
      for(let data of this.productsList)
      {
         if(data.id == this.id) {
            this.singleProductData = data;
            break;
         }
      }
   }

   public addToCart(value) {
      this.embryoService.addToCart(value);
   }

   public addToWishList(value) {
      this.embryoService.addToWishlist(value);
   }

   public getProduct (id) {

      this.embryoService.getProductsList().subscribe(res => {
      
         for(let data of res)
         {
            if(data.idProduit == this.id) {
               this.product = data;
               this.getStoresByProduct(this.product.idProduit);
               break;
            }
         }

      });
      

   }

   public getStoresByProduct (idProduit) {

      //this.Stores = this.embryoService.getStoresByProduct(idProduit)

      this.embryoService.getStoresByProduct(idProduit).subscribe(res => {
         this.Stores = res;
      });
      

   }


}
