import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Model/product';
import { Categorie } from 'src/app/Model/Categorie';
import { Marque } from 'src/app/Model/Marque';
import { BackService } from '../back.service';
import { Store } from 'src/app/Model/store';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {

  displayedColumns: string[] = ['idStore', 'nom', 'adresse', 'tel', 'edit' , 'delete' ];
  dataSource : any;
  popupResponse :any ;
  Add : boolean ; 
  edit : boolean;
  show : boolean;

  store : Store
  element : Store;

 constructor(private service:BackService) {
  this.store = new Store();
  this.store.adresse
   this.Add = false;
   this.edit = false;
   this.show = true;
   this.dataSource = []
    service.getStoresList().subscribe(res => {
     service.stores = res 
     this.dataSource = service.stores
   })
  }

 ngOnInit() {
 }

 toggleAdd():void {
   if (this.Add){
     this.Add = false 
     this.edit = false
     this.show = true
   }
   else {
     this.Add = true
     this.show=false
     this.edit= false 
     this.element = null 
   }
 }

 toggleditt(element : Store) {
  if (this.edit){
    this.edit = false 
    this.Add = false
    this.show=true
    this.element=null
  }
  else {
    this.show=false
    this.element = element;
    this.edit = true

  }
}

 AddStore (){
   this.store.idStore = 5454

   console.log(this.store)

  this.service.createStore(this.store).subscribe(res =>{
    console.log(res)
    this.service.calculatStoresCounts()
    this.store.nom='';
    this.store.tel = '';
    this.store.adresse ='';
  })

  this.toggleAdd();

 }

 public removeStore(value:any) {
   let message = "Are you sure you want to delete this Store?";
   this.service.confirmationPopup(message).
      subscribe(res => {this.popupResponse = res},
                err => console.log(err),
                ()  => this.getPopupResponse(this.popupResponse, value)
               );
}

public getPopupResponse(response, value) {
 if(response){
    this.service.removeLocalCartStore(value);
 }
}

editStore(){
  

  this.service.updateStore(this.element).subscribe(res =>{
    console.log(res)
    this.service.calculatStoresCounts()
    this.element=null
  })

  this.toggleditt(this.element);



}


}
