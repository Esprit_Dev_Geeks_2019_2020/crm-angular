import { Component, OnInit } from '@angular/core';
import { BackService } from '../back.service';
import { DashboardComponent } from '../dashboard/dashboard.component';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  menus = [];

  constructor(public sidebarservice: BackService,
    public Dashbord : DashboardComponent) {

      this.Dashbord.tab =  'Products'
    this.menus = sidebarservice.getMenuList();
    console.log(this.menus)
   }
 

  ngOnInit() {
  }


  toggleDashBoard(title){
    console.log(title);
    this.Dashbord.tab = title;



  }



  getSideBarState() {
    return this.sidebarservice.getSidebarState();
  }

  toggle(currentMenu) {
    if (currentMenu.type === 'dropdown') {
      this.menus.forEach(element => {
        if (element === currentMenu) {
          currentMenu.active = !currentMenu.active;
        } else {
          element.active = false;
        }
      });
    }
  }

  getState(currentMenu) {

    if (currentMenu.active) {
      return 'down';
    } else {
      return 'up';
    }
  }

  hasBackgroundImage() {
    return this.sidebarservice.hasBackgroundImage;
  }

}
