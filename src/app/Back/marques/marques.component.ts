import { Component, OnInit } from '@angular/core';
import { Marque } from 'src/app/Model/Marque';
import { BackService } from '../back.service';

@Component({
  selector: 'app-marques',
  templateUrl: './marques.component.html',
  styleUrls: ['./marques.component.css']
})
export class MarquesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nom', 'adresse', 'edit' , 'delete' ];
  dataSource : any;
  popupResponse :any ;
  Add : boolean ; 
  edit : boolean;
  show : boolean;

  marque : Marque
  element : Marque;

 constructor(private service:BackService) {
  this.marque = new Marque();
   this.Add = false;
   this.edit = false;
   this.show = true;
   this.dataSource = []
    service.getMarquesList().subscribe(res => {
     service.Marques = res 
     this.dataSource = service.Marques
   })
  }

 ngOnInit() {
 }

 toggleAdd():void {
   if (this.Add){
     this.Add = false 
     this.edit = false
     this.show = true
   }
   else {
     this.Add = true
     this.show=false
     this.edit= false 
     this.element = null 
   }
 }

 toggleditt(element : Marque) {
  if (this.edit){
    this.edit = false 
    this.Add = false
    this.show=true
    this.element=null
  }
  else {
    this.show=false
    this.element = element;
    this.edit = true

  }
}

AddMarque(){
   this.marque.id = 5454

   console.log(this.marque)

  this.service.createMarque(this.marque).subscribe(res =>{
    console.log(res)
    this.service.calculateMarquesCounts()
  })

  this.toggleAdd();

 }

 public removeMarque(value:any) {
   let message = "Are you sure you want to delete this Marque?";
   this.service.confirmationPopup(message).
      subscribe(res => {this.popupResponse = res},
                err => console.log(err),
                ()  => this.getPopupResponse(this.popupResponse, value)
               );
}

public getPopupResponse(response, value) {
 if(response){
    this.service.removeLocalCartMarque(value);
 }
}

editMarque(){
  

  this.service.updateMarque(this.element ).subscribe(res =>{
    console.log(res)
    this.service.calculateMarquesCounts()
    this.element=null
  })

  this.toggleditt(this.element);



}



}
