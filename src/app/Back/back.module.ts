import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BackRoutingModule } from './back-routing.module';
import { SideBarComponent } from '../Back/side-bar/side-bar.component';
import { DashboardComponent } from '../Back/dashboard/dashboard.component';
import { ProductsComponent } from '../Back/products/products.component';
import { StoresComponent } from '../Back/stores/stores.component';
import { MarquesComponent } from '../Back/marques/marques.component';
import { CategoriesComponent } from '../Back/categories/categories.component';
import { AgentsComponent } from '../Back/agents/agents.component';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, 
	MatCardModule, 
	MatMenuModule, 
	MatToolbarModule, 
	MatIconModule, 
	MatInputModule, 
	MatDatepickerModule, 
	MatNativeDateModule, 
	MatProgressSpinnerModule,
	MatTableModule, 
	MatExpansionModule, 
	MatSelectModule,
	MatSnackBarModule, 
	MatTooltipModule, 
	MatChipsModule, 
	MatListModule, 
	MatSidenavModule, 
	MatTabsModule, 
	MatProgressBarModule,
	MatCheckboxModule,
	MatSliderModule,
	MatRadioModule,
	MatDialogModule,
	MatGridListModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { backOfficeRoutes } from './back.routing';

// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';

// Import FusionCharts library
import * as FusionCharts from 'fusioncharts';

// Load FusionCharts Individual Charts
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// Use fcRoot function to inject FusionCharts library, and the modules you want to use
FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme)

import { QtePerStoreComponent } from '../Back/qte-per-store/qte-per-store.component';


@NgModule({
  declarations: [SideBarComponent, DashboardComponent, ProductsComponent, StoresComponent, MarquesComponent, CategoriesComponent, AgentsComponent, QtePerStoreComponent],
  imports: [
	FormsModule,
	FusionChartsModule,
    CommonModule,
    MatCardModule, 
		MatButtonModule,
		MatMenuModule, 
		MatToolbarModule, 
		MatIconModule, 
		MatInputModule, 
		MatDatepickerModule, 
		MatNativeDateModule, 
		MatProgressSpinnerModule,
		MatTableModule, 
		MatExpansionModule, 
		MatSelectModule, 
		MatSnackBarModule, 
		MatTooltipModule, 
		MatChipsModule, 
		MatListModule, 
		MatSidenavModule, 
		MatTabsModule, 
		MatProgressBarModule,
		MatCheckboxModule,
		MatSliderModule,
		MatRadioModule,
		MatDialogModule,
		MatGridListModule,
    BackRoutingModule
  ]
})
export class BackModule { }
