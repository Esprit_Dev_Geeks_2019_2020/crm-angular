import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/Model/Agent';
import { Store } from 'src/app/Model/store';
import { BackService } from '../back.service';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.css']
})
export class AgentsComponent implements OnInit {

  
  displayedColumns: string[] = ['idAgent', 'nom', 'prenom', 'cin', 'mail', 'edit' , 'delete' ];
  dataSource : any;
  popupResponse :any ;
  Add : boolean ; 
  edit : boolean;
  show : boolean;

  agent : Agent
  element : Agent;
  store = new Store();

 constructor(private service:BackService) {
  this.agent = new Agent();
  this.store = new Store();
   this.Add = false;
   this.edit = false;
   this.show = true;
   this.dataSource = []
    service.getAgentsList().subscribe(res => {
     service.Agents = res 
     this.dataSource = service.Agents
   })
   service.getStoresList().subscribe(res => {
    service.stores = res 
    this.dataSource = service.Agents
  })
   /*this.productForm = new  FormGroup({
    nom:new FormControl('',[Validators.required,Validators.minLength(5)]),
    description:new FormControl('',[Validators.required,Validators.minLength(7)]),
    categorie:new FormControl('',[Validators.required,Validators.minLength(5)]),
    marque:new FormControl('',[Validators.required,Validators.minLength(5)]),
    stock:new FormControl('',[Validators.required]),
    price:new FormControl('',[Validators.required]),
    
  });*/
  }

 ngOnInit() {
 }

 toggleAdd():void {
   if (this.Add){
     this.Add = false 
     this.edit = false
     this.show = true
   }
   else {
     this.Add = true
     this.show=false
     this.edit= false 
     this.element = null 
   }
 }

 ratingAttribute( value: any) {
   this.store = new Store();
  this.store.idStore = value;

  console.log(value)
}
 toggleditt(element : Agent) {
  if (this.edit){
    this.edit = false 
    this.Add = false
    this.show=true
    this.element=null
  }
  else {
    this.show=false
    this.element = element;
    this.edit = true

  }
}

 AddAgent (){

  console.log(this.store)

   this.agent.idAgent = 54541212

   this.agent.store = this.store

   this.store = new Store()

   console.log(this.agent)

  this.service.createAgent(this.agent).subscribe(res =>{
    console.log(res)
    this.service.calculateAgentCounts()
    this.agent.store=null
  })

  this.toggleAdd();

 }

 public removeAgent(value:any) {
   let message = "Are you sure you want to delete this Agent?";
   this.service.confirmationPopup(message).
      subscribe(res => {this.popupResponse = res},
                err => console.log(err),
                ()  => this.getPopupResponse(this.popupResponse, value)
               );
}

public getPopupResponse(response, value) {
 if(response){
    this.service.removeLocalCartAgent(value);
 }
}

editAgent(){
  
  this.agent.store = this.store;

  this.service.updateAgent(this.element , this.store).subscribe(res =>{
    console.log(res)
    this.service.calculateAgentCounts()
    this.element=null
  })

  this.toggleditt(this.element);



}


}
