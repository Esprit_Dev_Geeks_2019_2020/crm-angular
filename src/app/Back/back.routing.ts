import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';


export const backOfficeRoutes : Routes = [
     {
      path : '',
      component : DashboardComponent,
      children: [ 
         {
            path: 'products',
            component: ProductsComponent
         }
      ]
      }
   
]