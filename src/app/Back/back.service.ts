import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ToastaService, ToastaConfig, ToastOptions } from 'ngx-toasta';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Product } from '../Model/product';
import { ConfirmationPopupComponent } from '../Global/ConfirmationPopup/ConfirmationPopup.component';
import { Store } from '../Model/store';
import { Agent } from 'http';
import { Marque } from '../Model/Marque';
import { Categorie } from '../Model/Categorie';
import { QtePerStore } from '../Model/QtePerStore';

@Injectable({
  providedIn: 'root'
})
export class BackService {
  toggled = false;
  _hasBackgroundImage = true;
  menus = [
    {
      title: 'general',
      type: 'header'
    },
    {
      title: 'Products',
      icon: 'fa fa-book',
      active: false,
      type: 'simple',
    },
    {
      title: 'Stores',
      icon: 'fa fa-calendar',
      active: false,
      type: 'simple'
    },
    {
      title: 'Categories',
      icon: 'fa fa-calendar',
      active: false,
      type: 'simple'
    },
    {
      title: 'Marques',
      icon: 'fa fa-calendar',
      active: false,
      type: 'simple'
    },
    {
      title: 'Agents',
      icon: 'fa fa-calendar',
      active: false,
      type: 'simple'
    },
    {
      title: 'Products Distributions',
      icon: 'fa fa-folder',
      active: false,
      type: 'simple'
    }
  ];

  stores  :any ;

  Agents : any ;

  products : any ;

  productsPerMarque : any ;

  Categories : any ; 

  Marques : any ;

  QtePerStores : any ;

  products_path = 'crm/products/';
  stores_path = 'crm/stores/';
  categories_path = 'crm/categories/';
  marques_path = 'crm/marques/';
  agents_path = 'crm/agents/';
  qtes_path = 'crm/QtePerStore/';

 

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }


  constructor(private http:HttpClient,
    private dialog: MatDialog,
    private toastyService: ToastaService,
    private toastyConfig: ToastaConfig) {

    this.calculateProdCounts();
    this.calculateAgentCounts();
    this.calculatStoresCounts();
    this.calculateCategoriesCounts();
    this.calculateMarquesCounts();
    this.calculateQteCounts();

   }

   createItem(item): Observable<Product> {
    return this.http
      .post<Product>(this.products_path+"addProduct", JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  createQte(item): Observable<QtePerStore> {
    return this.http
      .post<QtePerStore>(this.qtes_path+"addProductStore", JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  createCategorie(item): Observable<Categorie> {
    return this.http
      .post<Categorie>(this.categories_path+"addCategorie", JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  createMarque(item): Observable<Marque> {
    return this.http
      .post<Marque>(this.marques_path+"addMarque", JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  createAgent(item): Observable<Agent> {
    return this.http
      .post<Agent>(this.agents_path+"addAgent", JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  createStore(item): Observable<Store> {
    return this.http
      .post<Store>(this.stores_path+"addStore", JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

   public calculateProdCounts() {

    this.products = [];
    
    this.getProductsList().subscribe(res => {
       this.products = res 
    })

 }

 public calculateProdPerMarque() {

  this.productsPerMarque = [];
  
  this.getProductsPerMarque().subscribe(res => {
     this.productsPerMarque = res 
  })

}

 public calculateQteCounts() {

  this.QtePerStores = [];
  
  this.getQteList().subscribe(res => {
     this.QtePerStores = res 
  })

}

 public calculateMarquesCounts() {

  this.Marques = [];
  
  this.getMarquesList().subscribe(res => {
     this.Marques = res 
  })

}

public calculateCategoriesCounts() {

  this.Categories = [];
  
  this.getCategoriesList().subscribe(res => {
     this.Categories = res 
  })

}


 public calculateAgentCounts() {

  this.Agents = [];
  
  this.getAgentsList().subscribe(res => {
     this.Agents = res 
  })

}

 public calculatStoresCounts() {

  this.stores = [];
  
  this.getStoresList().subscribe(res => {
     this.stores = res 
  })

}

  getProductsList(): Observable<Product> {
    return this.http
      .get<Product>(this.products_path+"getAllProducts")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  getProductsPerMarque(): Observable<Product> {
    return this.http
      .get<Product>(this.products_path+"getProductsPerMarque")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  getAgentsList(): Observable<Agent> {
    return this.http
      .get<Agent>(this.agents_path+"getAllAgents")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  getCategoriesList(): Observable<Categorie> {
    return this.http
      .get<Categorie>(this.categories_path+"getAllCategories")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  getQteList(): Observable<QtePerStore> {
    return this.http
      .get<QtePerStore>(this.qtes_path+"getAllQtePerStores")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  getMarquesList(): Observable<Marque> {
    return this.http
      .get<Marque>(this.marques_path+"getAllMarques")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  getStoresList(): Observable<Store> {
    return this.http
      .get<Store>(this.stores_path+"getAllStores")
      .pipe(
        retry(2),
        catchError(this.handleError )
      )
  }

  deleteItem(product) : Observable<Product> {
    
    return this.http
      .delete<Product>(this.products_path + 'deleteProduct?idProduit='+product.idProduit , this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteCategorie(categorie) : Observable<Categorie> {
    
    return this.http
      .delete<Categorie>(this.categories_path + 'deleteCategorie?idCategorie='+categorie.id , this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteQte(qte) : Observable<QtePerStore> {
    
    return this.http
      .delete<QtePerStore>(this.qtes_path + 'deleteProductStore?idQte='+qte.idQte , this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteMarque(marque) : Observable<Marque> {
    
    return this.http
      .delete<Marque>(this.marques_path + 'deleteMarque?idMarque='+marque.id , this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteAgent(Agent ) : Observable<Product> {
    
    return this.http
      .delete<Product>(this.agents_path + 'deleteAgent?idAgent='+Agent.idAgent , this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  deleteStore(store) : Observable<Store> {
    
    return this.http
      .delete<Store>(this.stores_path + 'deleteStore?idStore='+store.idStore , this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  updateItem( item): Observable<Product> {
    return this.http
      .put<Product>(this.products_path + 'updateProduct', JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateQte( item): Observable<QtePerStore> {
    return this.http
      .put<QtePerStore>(this.qtes_path + 'updateQtePerStore', JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateCategorie( item): Observable<Categorie> {
    return this.http
      .put<Categorie>(this.categories_path + 'updateCategorie', JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateMarque( item): Observable<Marque> {
    return this.http
      .put<Marque>(this.marques_path + 'updateMarque', JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateAgent( item , store): Observable<any> {
    return this.http
      .put<any>(this.agents_path + 'AffectAgent?idStore='+store.idStore, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateStore( store): Observable<Product> {
    return this.http
      .put<Product>(this.stores_path + 'updateStore', JSON.stringify(store), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  public confirmationPopup(message:string)
   {
      let confirmationPopup: MatDialogRef<ConfirmationPopupComponent>;
      confirmationPopup = this.dialog.open(ConfirmationPopupComponent);
      confirmationPopup.componentInstance.message = message;

      return confirmationPopup.afterClosed();
   }

   public removeLocalCartProduct(product: any) {
    
    this.deleteItem(product).subscribe(res => {
      console.log(res)
    })

    let toastOption: ToastOptions = {
       title: "Remove Product ",
       msg: "Product removing",
       showClose: true,
       timeout: 1000,
       theme: "material"
    };

    this.toastyService.wait(toastOption);
    setTimeout(() => {
       this.calculateProdCounts();
    }, 500);
 }

 public removeLocalCartQte(qte: any) {
    
  this.deleteQte(qte).subscribe(res => {
    console.log(res)
  })

  let toastOption: ToastOptions = {
     title: "Remove Product ",
     msg: "Product removing",
     showClose: true,
     timeout: 1000,
     theme: "material"
  };

  this.toastyService.wait(toastOption);
  setTimeout(() => {
     this.calculateQteCounts();
  }, 500);
}

 public removeLocalCartCategorie(categorie: any) {
    
  this.deleteCategorie(categorie).subscribe(res => {
    console.log(res)
  })

  let toastOption: ToastOptions = {
     title: "Remove Product ",
     msg: "Product removing",
     showClose: true,
     timeout: 1000,
     theme: "material"
  };

  this.toastyService.wait(toastOption);
  setTimeout(() => {
     this.calculateCategoriesCounts();
  }, 500);
}

 public removeLocalCartMarque(marque: any) {
    
  this.deleteMarque(marque).subscribe(res => {
    console.log(res)
  })

  let toastOption: ToastOptions = {
     title: "Remove Product ",
     msg: "Product removing",
     showClose: true,
     timeout: 1000,
     theme: "material"
  };

  this.toastyService.wait(toastOption);
  setTimeout(() => {
     this.calculateMarquesCounts();
  }, 500);
}
 

 public removeLocalCartAgent(agent: any) {
    
  this.deleteAgent(agent).subscribe(res => {
    console.log(res)
  })

  let toastOption: ToastOptions = {
     title: "Remove Agent",
     msg: "Agent removing",
     showClose: true,
     timeout: 1000,
     theme: "material"
  };

  this.toastyService.wait(toastOption);
  setTimeout(() => {
     this.calculateAgentCounts();
  }, 500);
}

 public removeLocalCartStore(store: any) {
    
      this.deleteStore(store).subscribe(res => {
        console.log(res)
      })

  let toastOption: ToastOptions = {
     title: "Remove Store",
     msg: "Store removing ",
     showClose: true,
     timeout: 1000,
     theme: "material"
  };

  this.toastyService.wait(toastOption);
  setTimeout(() => {
     this.calculatStoresCounts();
  }, 500);
}


  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


















  toggle() {
    this.toggled = ! this.toggled;
  }

  getSidebarState() {
    return this.toggled;
  }

  setSidebarState(state: boolean) {
    this.toggled = state;
  }

  getMenuList() {
    return this.menus;
  }

  get hasBackgroundImage() {
    return this._hasBackgroundImage;
  }

  set hasBackgroundImage(hasBackgroundImage) {
    this._hasBackgroundImage = hasBackgroundImage;
  }
}
