import { Component, OnInit } from '@angular/core';
import { BackService } from '../back.service';
import { Product } from 'src/app/Model/product';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Categorie } from 'src/app/Model/Categorie';
import { Marque } from 'src/app/Model/Marque';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  displayedColumns: string[] = ['idProduit', 'nom', 'stock', 'price', 'avialable','rating' , 'edit' , 'delete' ];
  dataSource : any;
  popupResponse :any ;
  Add : boolean ; 
  edit : boolean;
  show : boolean;

  product : Product
  element : Product;
  cat = new Categorie();
  marque  = new Marque()

 constructor(private service:BackService) {
  this.product = new Product();
  this.product.categorie = this.cat
  this.product.marque = this.marque
   this.Add = false;
   this.edit = false;
   this.show = true;
   this.dataSource = []
    service.getProductsList().subscribe(res => {
     service.products = res 
     this.dataSource = service.products
   })
   /*this.productForm = new  FormGroup({
    nom:new FormControl('',[Validators.required,Validators.minLength(5)]),
    description:new FormControl('',[Validators.required,Validators.minLength(7)]),
    categorie:new FormControl('',[Validators.required,Validators.minLength(5)]),
    marque:new FormControl('',[Validators.required,Validators.minLength(5)]),
    stock:new FormControl('',[Validators.required]),
    price:new FormControl('',[Validators.required]),
    
  });*/
  }

 ngOnInit() {
 }


 CategorieAttribute(value){
   this.cat = new Categorie();
   this.cat.id = value;
 }

 MarqueAttribute(value){
  this.marque = new Marque();
  this.marque.id = value;
}

 toggleAdd():void {
   if (this.Add){
     this.Add = false 
     this.edit = false
     this.show = true
   }
   else {
     this.Add = true
     this.show=false
     this.edit= false 
     this.element = null 
   }
 }

 toggleditt(element : Product) {
  if (this.edit){
    this.edit = false 
    this.Add = false
    this.show=true
    this.element=null
  }
  else {
    this.show=false
    this.element = element;
    this.edit = true

  }
}

 AddProduct (){
   this.product.idProduit = 5454

   this.product.categorie = this.cat
   this.product.marque = this.marque

   console.log(this.product)

  this.service.createItem(this.product).subscribe(res =>{
    console.log(res)
    this.service.calculateProdCounts()
    this.product.nom='';
    this.product.description = '';
    this.product.price=null
    this.product.stock=null
  })

  this.toggleAdd();

 }

 public removeProduct(value:any) {
   let message = "Are you sure you want to delete this product?";
   this.service.confirmationPopup(message).
      subscribe(res => {this.popupResponse = res},
                err => console.log(err),
                ()  => this.getPopupResponse(this.popupResponse, value)
               );
}

public getPopupResponse(response, value) {
 if(response){
    this.service.removeLocalCartProduct(value);
 }
}

editProduct(){
  
  console.log(this.element.stock)

  this.service.updateItem(this.element).subscribe(res =>{
    console.log(res)
    this.service.calculateProdCounts()
    this.element=null
  })

  this.toggleditt(this.element);



}


}
