import { Component, OnInit } from '@angular/core';
import { Categorie } from 'src/app/Model/Categorie';
import { BackService } from '../back.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nom', 'desc', 'edit' , 'delete' ];
  dataSource : any;
  popupResponse :any ;
  Add : boolean ; 
  edit : boolean;
  show : boolean;

  categorie : Categorie
  element : Categorie;

 constructor(private service:BackService) {
  this.categorie = new Categorie();
   this.Add = false;
   this.edit = false;
   this.show = true;
   this.dataSource = []
    service.getCategoriesList().subscribe(res => {
     service.Categories = res 
     this.dataSource = service.Categories
   })
  }

 ngOnInit() {
 }

 toggleAdd():void {
   if (this.Add){
     this.Add = false 
     this.edit = false
     this.show = true
   }
   else {
     this.Add = true
     this.show=false
     this.edit= false 
     this.element = null 
   }
 }

 toggleditt(element : Categorie) {
  if (this.edit){
    this.edit = false 
    this.Add = false
    this.show=true
    this.element=null
  }
  else {
    this.show=false
    this.element = element;
    this.edit = true

  }
}

 AddCategorie (){
   this.categorie.id = 5454

   console.log(this.categorie)

  this.service.createCategorie(this.categorie).subscribe(res =>{
    console.log(res)
    this.service.calculateCategoriesCounts()
  })

  this.toggleAdd();

 }

 public removeCategorie(value:any) {
   let message = "Are you sure you want to delete this Categorie?";
   this.service.confirmationPopup(message).
      subscribe(res => {this.popupResponse = res},
                err => console.log(err),
                ()  => this.getPopupResponse(this.popupResponse, value)
               );
}

public getPopupResponse(response, value) {
 if(response){
    this.service.removeLocalCartCategorie(value);
 }
}

editCategorie(){
  

  this.service.updateCategorie(this.element ).subscribe(res =>{
    console.log(res)
    this.service.calculateCategoriesCounts()
    this.element=null
  })

  this.toggleditt(this.element);



}


}
