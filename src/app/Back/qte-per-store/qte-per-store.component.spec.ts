import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QtePerStoreComponent } from './qte-per-store.component';

describe('QtePerStoreComponent', () => {
  let component: QtePerStoreComponent;
  let fixture: ComponentFixture<QtePerStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QtePerStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QtePerStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
