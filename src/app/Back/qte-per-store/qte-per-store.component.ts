import { Component, OnInit } from '@angular/core';
import { QtePerStore } from 'src/app/Model/QtePerStore';
import { Product } from 'src/app/Model/product';
import { Store } from 'src/app/Model/store';
import { BackService } from '../back.service';

@Component({
  selector: 'app-qte-per-store',
  templateUrl: './qte-per-store.component.html',
  styleUrls: ['./qte-per-store.component.css']
})
export class QtePerStoreComponent implements OnInit {

  displayedColumns: string[] = ['idQte', 'product', 'store', 'qte' , 'edit' , 'delete' ];
  dataSource : any;
  popupResponse :any ;
  Add : boolean ; 
  edit : boolean;
  show : boolean;

  product : Product
  store : Store
  qte : QtePerStore;
  element : QtePerStore;

 constructor(private service:BackService) {
  this.product = new Product();
  this.qte = new QtePerStore()
  this.store = new Store();
   this.Add = false;
   this.edit = false;
   this.show = true;
   this.dataSource = []
    service.getQteList().subscribe(res => {
     service.QtePerStores = res 
     this.dataSource = service.QtePerStores
   })
  }

 ngOnInit() {
 }


 ProductAttribute(value){
   this.product = new Product();
   this.product.idProduit = value;
 }

 StoreAttribute(value){
  this.store = new Store();
  this.store.idStore = value;
}

 toggleAdd():void {
   if (this.Add){
     this.Add = false 
     this.edit = false
     this.show = true
   }
   else {
     this.Add = true
     this.show=false
     this.edit= false 
     this.element = null 
   }
 }

 toggleditt(element : QtePerStore) {
  if (this.edit){
    this.edit = false 
    this.Add = false
    this.show=true
    this.element=null
  }
  else {
    this.show=false
    this.element = element;
    this.edit = true

  }
}

 AddQte (){
   this.qte.idQte = 5454

   this.qte.product = this.product
   this.qte.store = this.store

   console.log(this.qte)

  this.service.createQte(this.qte).subscribe(res =>{
    console.log(res)
    this.service.calculateQteCounts()
    this.product=new Product();
    this.store = new Store();
    this.qte.qte=null
  })

  this.toggleAdd();

 }

 public removeQte(value:any) {
   let message = "Are you sure?";
   this.service.confirmationPopup(message).
      subscribe(res => {this.popupResponse = res},
                err => console.log(err),
                ()  => this.getPopupResponse(this.popupResponse, value)
               );
}

public getPopupResponse(response, value) {
 if(response){
    this.service.removeLocalCartProduct(value);
 }
}

editQte(){
  
  console.log(this.element)

  this.service.updateQte(this.element).subscribe(res =>{
    console.log(res)
    this.service.calculateQteCounts()
    this.element=null
  })

  this.toggleditt(this.element);



}


}
