import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';

const   backOfficeRoutes : Routes = [
  {
   path : '',
   component : DashboardComponent,
  }
]

@NgModule({
  imports: [RouterModule.forChild(backOfficeRoutes)],
  exports: [RouterModule]
})
export class BackRoutingModule { }
