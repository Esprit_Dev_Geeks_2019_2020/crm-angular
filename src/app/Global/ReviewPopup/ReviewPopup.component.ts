import { Component, OnInit ,Input} from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { EmbryoService } from '../../Services/Embryo.service';
import { Rating } from 'src/app/Model/Rating';
import { Product } from 'src/app/Model/product';
import { User } from 'src/app/Model/User';

@Component({
  selector: 'app-ReviewPopup',
  templateUrl: './ReviewPopup.component.html',
  styleUrls: ['./ReviewPopup.component.scss']
})
export class ReviewPopupComponent implements OnInit {

   product : any ;

   pro  : Product ;

   user : User ; 

   ratings : any ;

   Rating : Rating;

   rating : any;
  
  quantityArray : number[] = [1,2,3,4,5];

   embryoService: EmbryoService
   singleProductDetails : any;
   reviews : any;

  

   constructor(public dialogRef: MatDialogRef<ReviewPopupComponent> ) { 

    this.Rating = new Rating();

  
   }

   ngOnInit() {
   }

   onKey(event : any ){
    this.Rating.review = event.target.value;
   }

   public ratingAttribute( value: any) {
    this.rating = value;
    this.Rating.rating = value;
 }

 public setRating ( ){
   console.log("here in popup ")
   this.user = new User()

   this.user.idPersonne = 1;

   this.Rating.user = this.user

   this.pro = new Product()

   this.pro.idProduit = this.singleProductDetails.idProduit;

    this.Rating.product = this.pro

   this.Rating.product = this.pro
   this.embryoService.setRating(this.Rating).subscribe((response) => {
    this.ratings = response
  });
 }

}
