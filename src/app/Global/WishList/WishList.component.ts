import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { EmbryoService } from 'src/app/Services/Embryo.service';

@Component({
  selector: 'embryo-WishList',
  templateUrl: './WishList.component.html',
  styleUrls: ['./WishList.component.scss']
})
export class WishListComponent implements OnInit, OnChanges {

   @Input() wishListProducts : any;

   @Input() wishes : any ;

   wishs : any;

   @Input() count        : number;

   @Input() currency      : string;

   @Output() removeWishListData : EventEmitter<any> = new EventEmitter();

   @Output() addAllWishlistToCart : EventEmitter<any> = new EventEmitter();

   @Output() addToCart: EventEmitter<any> = new EventEmitter();

   hiddenBadge = true;

   constructor(public embryoService: EmbryoService) {
      
      this.wishes = []

      this.embryoService.getLovedProducts(1).subscribe((response) => {
         this.wishs = response
         console.log(this.wishs)
       });

    }

   ngOnInit() {
      console.log(this.wishes)

   }

   ngOnChanges() {
      if(this.count && this.count != 0) {
         this.hiddenBadge = false;
      } else {
         this.hiddenBadge = true;
      }
   }

   public confirmationPopup(product:any) {
      this.removeWishListData.emit(product);
   }

   public addAllToCart() {
      console.log(this.wishListProducts.length)
      console.log(this.count)
      this.addAllWishlistToCart.emit(this.wishListProducts);
   }

   public calculatePrice(product) {
      let total = null;
      total = product.price*product.quantity;
      return total;
   }

   public addToCartProduct(product) {
      this.addToCart.emit(product);
   }

}
