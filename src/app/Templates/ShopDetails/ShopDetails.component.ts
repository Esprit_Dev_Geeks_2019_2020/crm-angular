import { Component, OnInit, Input, OnChanges, Renderer2, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
declare var $: any;

import {EmbryoService } from '../../Services/Embryo.service';
import { WishProduct } from 'src/app/Model/WishProduct';
import { Product } from 'src/app/Model/product';
import { User } from 'src/app/Model/User';

@Component({
  selector: 'embryo-ShopDetails',
  templateUrl: './ShopDetails.component.html',
  styleUrls: ['./ShopDetails.component.scss']
})
export class ShopDetailsComponent implements OnInit, OnChanges {

   @Input() detailData : any;
   @Input() currency   : string;

   wish : WishProduct ;

   produit  : Product ;

   personne : User ; 

    wishs : any ;

   mainImgPath   : string;
   totalPrice    : any;
   type          : any;
   colorsArray   : string[] = ["Red", "Blue", "Yellow", "Green"];
   sizeArray     : number[] = [36,38,40,42,44,46,48];
   quantityArray : number[] = [1,2,3,4,5,6,7,8,9,10];
   productReviews : any;

   constructor(private route: ActivatedRoute,
               private router: Router, 
               public embryoService : EmbryoService
               ) {
      this.embryoService.getProductReviews().valueChanges().subscribe(res => {this.productReviews = res});
      this.embryoService.getLovedProducts(1).subscribe((response) => {
         this.wishs = response
         console.log(this.wishs)
       });
       this.wish = new WishProduct()
   }

   ngOnInit() {
     // this.mainImgPath = this.detailData.image;
      this.totalPrice  = this.detailData.price; 

      this.route.params.subscribe(res => {
         this.type = null;
         this.type = res.type; 
      });
   }

   ngOnChanges() {
      this.mainImgPath = null;
      this.totalPrice  = null;
      this.mainImgPath = this.detailData.image;
      this.totalPrice  = this.detailData.price; 
   }

   /**
    * getImagePath is used to change the image path on click event. 
    */
   public getImagePath(imgPath: string, index:number) {
      $('.p-link').removeClass('border-active');
      this.mainImgPath = imgPath;
      $("#"+index+"_img").addClass('border-active');
   }

   public calculatePrice(detailData:any, value: any) {
      detailData.quantity = value;
      this.totalPrice = detailData.price*value;
   }

   public reviewPopup(detailData) {
      let reviews : any = null;
      for(let review of this.productReviews) {
         // if((review.id == detailData.id) && (review.type == detailData.type) && (review.category == detailData.category)){
         //    singleProduct = review;
         //    break;
         // }

        reviews = review.user_rating;
      }

      this.embryoService.reviewPopup(detailData, reviews);
   }

   public addToWishlist(value:any) {
      

      
      this.produit = new Product()

      this.produit.idProduit = this.detailData.idProduit

      this.personne = new User()
      this.personne.idPersonne = 1

      this.wish.idWish = 1
      this.wish.produit = this.produit
      this.wish.personne = this.personne
      console.log(this.wish)
      this.embryoService.AddToWished(this.wish).subscribe((response) => {
         console.log(response)
       });

       this.embryoService.addToWishlist(value);
   }

   public addToCart(value:any) {
      this.embryoService.addToCart(value);
   }

   public buyNow(value:any) {
      this.embryoService.buyNow(value);
      this.router.navigate(['/checkout'])
      ;
   }

}
