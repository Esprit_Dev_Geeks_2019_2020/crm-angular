import { Component, OnInit, Input, Output, EventEmitter , SimpleChanges} from '@angular/core';
import { EmbryoService } from '../../../Services/Embryo.service';
import { WishProduct } from 'src/app/Model/WishProduct';
import { Product } from 'src/app/Model/product';
import { User } from 'src/app/Model/User';
declare var $: any;

@Component({
  selector: 'embryo-ProductGrid',
  templateUrl: './ProductGrid.component.html',
  styleUrls: ['./ProductGrid.component.scss']
})
export class ProductGridComponent implements OnInit {

   wish : WishProduct ;

   produit  : Product ;

   personne : User ; 

   @Input() products : any ;

   @Input() attributte : any ;

   productList : any ;

   @Input() currency : string;

   @Input() gridLength : any;

   @Input() gridThree : boolean = false;

   @Output() addToCart: EventEmitter<any> = new EventEmitter();

   @Output() addToWishList: EventEmitter<any> = new EventEmitter();

   loaded = false;
   lg     = 25;
   xl     = 25;

   trackByObjectID(index, hit) {
      return hit.objectID;
   }

   constructor(public embryoService: EmbryoService) { 
      this.productList = []
      this.getAllProducts();
      

   }

   ngOnInit() {

      if(this.gridThree) {
         this.lg = 33;
         this.xl = 33;
      }

     
   }

   ngOnChanges (changes: SimpleChanges): void {
      for (let propName in changes) {
        let change = changes[propName];
        this.productList =this.embryoService.getProductsBySearch(change.currentValue);}
  
    }

   public addToCartProduct(value:any) {
      this.addToCart.emit(value);
   }

   public onLoad() {
      this.loaded = true;
   }

   public productAddToWishlist(value:any, parentClass) {
      if(!($('.'+parentClass).hasClass('wishlist-active'))) {
         $('.'+parentClass).addClass('wishlist-active');
      }
      this.addToWishList.emit(value);
   }

   public checkCartAlready(singleProduct) {
      let products = JSON.parse(localStorage.getItem("cart_item")) || [];
      if (!products.some((item) => item.name == singleProduct.name)) {
         return true;
      }
   }

   getAllProducts() {
      //Get saved list of products
      this.embryoService.getProductsList().subscribe( response => {
        this.productList = response;
      })
    }

    public addToWishlist(value:any) {
      

      
      this.produit = new Product()

      this.produit.idProduit = value.idProduit

      this.personne = new User()
      this.personne.idPersonne = 1

      this.wish.idWish = 1
      this.wish.produit = this.produit
      this.wish.personne = this.personne
      console.log(this.wish)
      this.embryoService.AddToWished(this.wish).subscribe((response) => {
         console.log(response)
       });

       this.embryoService.addToWishlist(value);
   }

}
