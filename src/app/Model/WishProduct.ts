import { Product } from "./product";
import { User } from "./User";

export class WishProduct {
   produit :Product;
   personne: User;
   idWish : number;
}
