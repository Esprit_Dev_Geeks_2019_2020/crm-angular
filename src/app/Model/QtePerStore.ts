import { Product } from "./product";
import { Store } from "./store";

export class QtePerStore {
    idQte : number;
    product : Product
    store : Store
    qte : number
    avialable : string;

}