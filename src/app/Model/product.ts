import { Categorie } from "./Categorie";
import { Marque } from "./Marque";

export class Product {
    idProduit :number;
    categorie : Categorie;
    price :number ; 
    rating  : number;
    devise : string;
    color : string ; 
    description : string ; 
    nom : string ; 
    stock : number;
    qtes = []
    img = null ;
    avialable : string; 
    objectID  : number ; 
    marque  : Marque
 }