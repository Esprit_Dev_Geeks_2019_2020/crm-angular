import { Product } from "./product";
import { User } from "./User";

export class Rating {
   idRating: number;
   product :Product;
   rating: number;
   user: User;
   review: string;
}
